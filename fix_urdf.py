import cv2
import cv2.aruco as aruco
import numpy as np
import yaml
from scipy.spatial.transform import Rotation as R

import klampt
from klampt import vis
from klampt import Geometry3D
from klampt.math import vectorops,so3,se3
import time


'''
Klamp Visualization
'''
# add the agv to the world
world = klampt.WorldModel()
world.loadElement("agv_driver.urdf")
robot = world.robot(0)


# add the world to the visualizer
vis.add("world",world)

# add transformation matrices
xform = se3.identity()
vis.add("marker",xform)


vis.show()
time.sleep(1000)


