import cv2
import cv2.aruco as aruco
import numpy as np
import yaml

import klampt
from klampt import vis
from klampt.math import vectorops,so3,se3
import time


# Helper: rotation and translation vector to transformation matrix #
def get_transf_mtx(rvec, tvec):
    rmtx, _ = cv2.Rodrigues(rvec)
    tmtx = np.append(rmtx, np.squeeze(tvec).reshape(3, 1), axis=1)
    tmtx = np.append(tmtx, np.array([[0, 0, 0, 1]]), axis=0)
    return tmtx


'''
Aruco marker pose estimation 
'''
# initializations
marker_size = 0.1
aruco_dict = aruco.Dictionary_get(dict=aruco.DICT_6X6_250)
params = aruco.DetectorParameters_create()
params.adaptiveThreshWinSizeMin = 5
params.adaptiveThreshWinSizeMax = 5
params.cornerRefinementMethod = aruco.CORNER_REFINE_CONTOUR # CONTOUR works better than SUBPIX in terms of the std of the positions
params.cornerRefinementMaxIterations = 10000
params.cornerRefinementMinAccuracy = 0.0001

# load image
img = cv2.imread("marker.jpeg")

# get markers
corners, ids, _ = aruco.detectMarkers(img, aruco_dict, parameters=params)

# camera matrix and distortion matrix
camera_stat_dict = None
with open("agv_driver/static/camera_stat.yaml", 'r') as f:
    camera_stat_dict = yaml.full_load(f)
camera_matrix = np.array(camera_stat_dict['jhcap']['Calibration Matrix']['2048x1536']['cameraMatrix'])
dist_matrix = np.array(camera_stat_dict['jhcap']['Calibration Matrix']['2048x1536']['distCoeffs'])

# estimate transformation
marker_corners = corners[ids.tolist().index([4])]
marker_rvec, marker_tvec, _ = aruco.estimatePoseSingleMarkers(marker_corners,
                                                              marker_size,
                                                              camera_matrix,
                                                              dist_matrix)

trans_matrix = get_transf_mtx(marker_rvec, marker_tvec)   # camera is origin, marker in camera-frame  (marker to camera) T_mc
trans_inv = np.linalg.inv(trans_matrix)                   # marker is origin, camera in marker-frame  (camera to marker) T_cm


'''
Klamp Visualization
'''
# add the agv to the world
world = klampt.WorldModel()
world.loadElement("agv_driver.urdf")
robot = world.robot(0)
robot.setConfig([0, -0.8, 0, -1.57, 0, 0])  # manually set a configuration for the agv to match the camera position (subject to change)

# solve for the transformation from agv to camera
T_cm = se3.from_ndarray(trans_inv)
T_rm = robot.link(5).getTransform()   # read the transformation from current robot position
T_rc = se3.mul(se3.inv(T_cm), T_rm)   # T_rc = T_mc @ T_rm (camera in robot-frame)
print(T_rc)

# add the world to the visualizer
vis.add("world", world)

# add transformation matrices
xform = se3.identity()
vis.add("marker", xform)
vis.add("camera", T_cm)
vis.add("robot", T_rm)
vis.add("robot_2", se3.mul(T_cm, T_rc))

vis.show()
time.sleep(1000)


